# express-boilerplate
## What is this? 
Simple boilerplate for Express + MongoDB project. It may uses in MEAN, MERN, MEVN stack.

## Includes:
* Express
* Mongoose (MongoDB driver)
* Cors
* Morgan (logging) + RFS (rotating log files writting)
* Helmet
* dotenv (.env support)

## How to use
1. Install dependencies:
> Yarn is strongly recommended instead of npm.
```bash
yarn
```

2. Create **.env** from **.env.example**. `PORT` and `DB_URL` is required variables.

3. Start server in development mode with **nodemon**:

```bash
yarn dev
```

## Implemented endpoints
| Method        | Endpoint         |
| ------------- |:----------------:|
| **GET**       | /posts/get       |
| **GET**       | /posts/get/:id   |
| **POST**      | /posts/create    |
| **PUT**       | /posts/update    |
| **DELETE**    | /posts/delete    |



