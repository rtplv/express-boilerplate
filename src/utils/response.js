/**
 * Success response
 */
exports.success = (res, resData) => res.status(200).send({
  success: true,
  ...resData,
});

/**
 * Errors handlers
 */
exports.errors = {
  // Client error's
  unauthorized(res) {
    return res.status(401).send({
      success: false,
      error: 'Access denied. Authorization required.',
    });
  },
  forbidden(res) {
    return res.status(403).send({
      success: false,
      error: 'Access to the resource is prohibited.',
    });
  },
  notFound(res) {
    return res.status(404).send({
      success: false,
      error: 'Not found.',
    });
  },
  // Server error's
  internalError(res, error) {
    return res.status(500).send({
      success: false,
      error: 'Internal server error.',
      debug: error,
    });
  }
};
