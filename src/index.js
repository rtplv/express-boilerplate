// env variables add
require('dotenv').config();

// App constant from .env
const { PORT, DB_URL } = process.env;

// Base libraries
const fs = require('fs');
const path = require('path');

// Express and plugins
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');
const { generator } = require('./utils/logs');
const helmet = require('helmet');

/* Add logging */
const logDirectory = path.join(__dirname,'..', 'logs');
// Create directory if it not exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// Create rotate file stream
const accessLogStream = rfs(generator, {
  size: '10M',
  interval: '1d', // rotate daily
  path: logDirectory,
});

/* Init app */
const app = express();

// Add assets
app.use(morgan('combined', { stream: accessLogStream }));
app.use(express.json()); // replaced a body-parser
app.use(cors());
app.use(helmet());

// Routes
require('./routes').applyTo(app);

// Connect to db and create app
(async () => {
  try {
    await mongoose.connect(DB_URL);
    console.log(`💾 Connect with MongoDB on ${DB_URL} port is success!`);

    app.listen(PORT, () => console.log(`👍 Server running on ${ PORT } port`));
  } catch (err) {
    throw new Error(err)
  }
})()
