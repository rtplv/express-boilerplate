const Post = require('../models/Post');
const Response = require('../utils/response');

/**
 * Get all posts
 * @param req
 * @param res
 */
module.exports.getAll = async (req, res) => {
  try {
    const posts = await Post.find()
      .select('title description _id')
      .sort({ _id: -1 })
      .exec();

    return Response.success(res, { posts });
  } catch (err) {
    return Response.errors.internalError(res, err)
  }
};

/**
 * Find and return post by id
 * @param req
 * @param res
 */
module.exports.getPostById = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id, 'title description');

    return Response.success(res, { post });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Create new post
 * @param req
 * @param res
 */
module.exports.createPost = async (req, res) => {
  const newPost = new Post({
    title: req.body.title,
    description: req.body.description,
  });

  try{
    const createdPost = await newPost.save();

    return Response.success(res, {
      message: `Post with ID ${createdPost._id} successfully saved`,
      createdPost,
    });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Update exists post
 * @param req
 * @param res
 */
module.exports.updatePost = async (req, res) => {
  try {
    const post = await Post.findById(req.body.id);

    // Update fields
    if (req.body.title) {
      post.title = req.body.title;
    }
    if (req.body.description) {
      post.description = req.body.description;
    }
    // Update post
    try{
      const updatedPost = await post.save();

      return Response.success(res, {
        message: `Post with ID ${updatedPost._id} successfully updated`,
        updatedPost,
      });
    } catch (err) {
      return Response.errors.internalError(res, err);
    }
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};

/**
 * Delete post by id
 * @param req
 * @param res
 */
module.exports.deletePost = async (req, res) => {
  try {
    const deletedPost = await Post.findOneAndRemove({ _id: req.body.id });
    return Response.success(res, {
      message: `Post with ID ${deletedPost._id} successfully deleted`,
      deletedPost
    });
  } catch (err) {
    return Response.errors.internalError(res, err);
  }
};
