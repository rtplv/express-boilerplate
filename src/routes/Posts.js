const express = require('express');
const router = express.Router();

const PostsController = require('../controllers/PostsController');

router.get('/posts/get', PostsController.getAll);
router.get('/posts/get/:id', PostsController.getPostById);

router.post('/posts/create', PostsController.createPost);
router.put('/posts/update', PostsController.updatePost);
router.delete('/posts/delete', PostsController.deletePost);

module.exports = router;
