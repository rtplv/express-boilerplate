/**
 * Apply all exists routes to app
 * @param app
 */
exports.applyTo = app => {
  app.use(require('./Posts'));
};
